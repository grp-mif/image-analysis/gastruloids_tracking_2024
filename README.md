# Project summary

This project is about tracking cells in gastruloids imaged on Viventis microscope.

# Desired measurements

Running the analysis pipeline, we aim to understand, ...

# Data management

- [Data management](data_management.md)

# Workflow

- [Analysis workflow](analysis_workflow.md)

## Usage instructions

- Download this repository and store on your lab server.
- Codes are stored in `code/python` folder.
Run the scripts as follows:
Protocol for Viventis image processing and cell tracking.

1. run `preprocessing\01_image_based_registration.py`

   point cloud-based registration
   
2. run `preprocessing\02_make_tophat.py`

   run background correction with tophat and gaussian
   
3. run TGMM

4. Import TGMM tracks into Mastodon (FIJI) and compute track properties.

- run `track_analysis.ipynb` or `plot_conditions/track_analysis_compare_conditions.ipynb` for some tranck quantification.

# For developers

**NOTE:** To include screenshots to the readme, please put them in the "documentation" folder.

To make videos to be added to Gitlab, see [instructions](https://git.embl.de/grp-cba/cba/-/blob/master/misc/make-video-from-tif-fiji.md).

### Installation instruction:

This software depends on the following library:
- [pyclesperanto-prototype](https://github.com/clEsperanto/pyclesperanto_prototype): for GPU-accelerated image processing.

To install the packages, please follow the instructions in the pages.

### How to update repository:

- make changes
- in terminal inside the repository folder type `git add --all`
- commit: type `git commit -m "a message"`
- type: `git push origin main`

How to take changes from repository to local computer:
- in terminal inside repository folder, type `git pull`
