
files = dir('*.svb');
filenames = strings(length(files),1);
for i=1:length(files)
    filenames(i,1) = files(i).name;
end

for j=1:length(filenames)
    
    filename = filenames(j);
    [svList, sizeIm] = readListSupervoxelsFromBinaryFile( filename );

    supervoxel_positions = cell(size(svList,1),1);

    for i=1:size(svList,1)
        [x,y,z] = ind2sub(sizeIm, svList{i});
        supervoxel_positions{i,1} = [x,y,z];
    end
    
    outname = extractBefore(filename, '.svb') + '.mat';
    save(outname, 'supervoxel_positions');

end
