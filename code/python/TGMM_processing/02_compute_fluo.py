# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 09:43:40 2021

@author: nicol
"""

import glob, os, tqdm
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
from matplotlib import animation
from skimage.io import imread, imsave
from scipy.io import loadmat
import multiprocessing.pool as mpp
import sys

##################################################
img_folders = [
    [
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/mKO2_reg_tophat_gauss'),
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/GFP_reg'),
    ],
    [
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/mKO2_reg1'),
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/GFP_reg1'),
    ],
    [
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '2021_PD_72h/20210429_pos3_72h_reg_g/mKO2_reg_tophat_gauss'),
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '2021_PD_72h/20210429_pos3_72h_reg_g/GFP_reg'),
    ],
    [
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '2021_PD_72h/20210429_pos3_72h_reg_g/mKO2'),
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '2021_PD_72h/20210429_pos3_72h_reg_g/GFP'),
    ]
    [
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20220204_a2imix_96h_LC/Pos3_reg_g/mKO2_reg_tophat_gauss'),
        os.path.join('Y:\\',os.sep,
            'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
            '20220204_a2imix_96h_LC/Pos3_reg_g/GFP_reg'),
    ],
]
tgmm_folders = [
    os.path.join('Y:\\',os.sep,
                 'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
                 '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/TGMM/GMEMtracking3D_2022_2_7_ESL_48h_reg'),
    os.path.join('Y:\\',os.sep,
                 'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
                 '20211110_ESLmix_48h_firstTex_probnochi/Pos3_best_reg_g/TGMM/GMEMtracking3D_2022_2_11_ESL_48h_reg1'),
    os.path.join('Y:\\',os.sep,
                 'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
                 '2021_PD_72h/20210429_pos3_72h_reg_g/TGMM/GMEMtracking3D_2022_2_7_PD_72h_reg'),
    os.path.join('Y:\\',os.sep,
                 'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
                 '2021_PD_72h/20210429_pos3_72h_reg_g/TGMM/GMEMtracking3D_2022_2_8_PD_72h_noreg'),
    os.path.join('Y:\\',os.sep,
                 'Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess',
                 '20220204_a2imix_96h_LC/Pos3_reg_g/TGMM/GMEMtracking3D_2022_2_7_a2i_96h'),
    ]

img_folder = img_folders[1]
tgmm_folder = tgmm_folders[1]

df = pd.read_csv(os.path.join(tgmm_folder,'tracks.csv'))

mko2_list = glob.glob(os.path.join(img_folder[0], '*.tif'))
mko2_list.sort()
gfp_list = glob.glob(os.path.join(img_folder[1], '*.tif'))
gfp_list.sort()

####### load supervoxels
print('##### Loading supervoxels')
matlist = glob.glob(os.path.join(tgmm_folder, 'XML_finalResult_lht', '*.mat'))
matlist.sort()
supervoxels = { 'tp'+str(i):[] for i in range(len(matlist)) }
for i, mat in tqdm.tqdm(enumerate(matlist), total=len(matlist)):
    poss = loadmat(mat)['supervoxel_positions']
    for pos in poss:
        supervoxels['tp'+str(i)].append(pos[0])

####### test
# a = df.loc[0]
# tp = int(a.tp)
# img1 = imread(mko2_list[tp])
# img2 = imread(gfp_list[tp])
# svidx = int(a.svIdx)
# voxels = supervoxels['tp'+str(tp)][svidx]
# zlims = [np.min(voxels[:,2]), np.max(voxels[:,2])]
# ylims = [np.min(voxels[:,1]), np.max(voxels[:,1])]
# xlims = [np.min(voxels[:,0]), np.max(voxels[:,0])]
# cellimg1 = img1[zlims[0]:zlims[1],ylims[0]:ylims[1],xlims[0]:xlims[1]]
# cellimg2 = img2[zlims[0]:zlims[1],ylims[0]:ylims[1],xlims[0]:xlims[1]]
# print(tp)
# z=int(a.z)
# y=int(a.y)
# x=int(a.x)
# fig, ax = plt.subplots(1,4)
# w = 13
# ax[0].imshow(img1[z,y-w:y+w,x-w:x+w])
# ax[0].scatter(w,w,c='w')
# ax[1].imshow(img2[z,y-w:y+w,x-w:x+w])
# ax[1].scatter(w,w,c='w')
# ax[2].imshow(cellimg1[int(cellimg1.shape[0]/2)])
# ax[3].imshow(cellimg2[int(cellimg2.shape[0]/2)])

####### compute mko2 and gfp signal for every cell
for img_list, key in zip([mko2_list, gfp_list], ['mKO2', 'GFP']):
    imgs = [ imread(i) for i in tqdm.tqdm(img_list, total=len(img_list)) ]
    vals = []
    for i, row in tqdm.tqdm(df.iterrows(), total=len(df)):
        tp = int(row.tp)
        svidxs = np.array(row.svIdx.split(' ')[:-1]).astype(int)
        voxels = [supervoxels['tp'+str(tp)][svidx] for svidx in svidxs]
        voxels = np.concatenate(voxels)-1
        val = imgs[tp][voxels[:,2],voxels[:,1],voxels[:,0]]
        vals.append(np.mean(val))
    
    df[key] = vals

df.to_csv(os.path.join(tgmm_folder,'tracks_withFluo.csv'), index = False)

# ####### visualize
# df = pd.read_csv('tracks_%s_reg_%sh_withFluo.csv'%(tracking_channel,time_window))

# df.z = df.z*scale

# def _set_axes_radius(ax, origin, radius):
#     x, y, z = origin
#     ax.set_xlim3d([x - radius, x + radius])
#     ax.set_ylim3d([y - radius, y + radius])
#     ax.set_zlim3d([z - radius, z + radius])
    
# def set_axes_equal(ax: plt.Axes):
#     """Set 3D plot axes to equal scale.

#     Make axes of 3D plot have equal scale so that spheres appear as
#     spheres and cubes as cubes.  Required since `ax.axis('equal')`
#     and `ax.set_aspect('equal')` don't work on 3D.
#     """
#     limits = np.array([
#         ax.get_xlim3d(),
#         ax.get_ylim3d(),
#         ax.get_zlim3d(),
#     ])
#     origin = np.mean(limits, axis=1)
#     radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
#     _set_axes_radius(ax, origin, radius)
    
# def visualize_tl_cells(cells_tl, elev=30, azim_init=-75, interval=10, cmap='plasma'):
#     from matplotlib.animation import FuncAnimation
#     ### visualize development from cell tracks

#     t_idx = list(set(cells_tl.tp))
#     t_idx.sort()
    
#     cells_tp = cells_tl[cells_tl.tp==0]
#     fig = plt.figure(figsize=(8,8))
#     ax = fig.add_subplot(projection='3d')
#     scatter = ax.scatter(cells_tp.x, cells_tp.y, cells_tp.z, linewidth=0, s=50, 
#                          alpha=.5, c=cells_tp['GFP'], cmap=cmap)
#     title = ax.set_title('time={}'.format(0), fontsize=60)
#     # ax.set_xlim(0,2000)
#     # ax.set_ylim(0,2000)
#     # ax.set_zlim(0,2000)
#     ax.set_box_aspect([1,1,1])
#     set_axes_equal(ax)
#     ax.view_init(elev=elev, azim=azim_init)
    
#     def update(frame):
#         cells_tp = cells_tl[cells_tl.tp==frame]
#         t = cells_tp.tp.values[0]
#         scatter._offsets3d = (cells_tp.x, cells_tp.y, cells_tp.z)
#         scatter.set_array(cells_tp['GFP'])
#         title.set_text('time={}'.format(t))
        
#     ani = FuncAnimation(fig, update, len(t_idx), 
#                                   interval=interval, blit=False)
    
#     return ani

# # ani = visualize_tl_cells(df, elev=30, azim_init=-75)
# # writervideo = animation.FFMpegWriter(fps=6) 
# ## ani.save('movie_timelapse_%s_%sh.gif'%(tracking_channel,time_window), writer=writervideo)
