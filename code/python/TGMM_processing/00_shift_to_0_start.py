# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 09:43:40 2021

@author: nicol
"""

import glob, os, tqdm
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
from matplotlib import animation
from skimage.io import imread, imsave
from scipy.io import loadmat
import multiprocessing.pool as mpp
import sys

##################################################
master_folder = os.path.join('Y:','/Kerim_Anlas/gastruloid_imaging/tracking_preprocess/preprocess')

exp_folders = [
    [
        os.path.join(master_folder,
            '20211118_ESLmix_96h_good\\Pos1_goodfewcells_reg_g/mKO2_reg1'),
        os.path.join(master_folder,
            '20211118_ESLmix_96h_good\\Pos1_goodfewcells_reg_g/GFP_reg1'),
    ],
]

import re

for exp_folder in exp_folders:

    
    for extension in ['*.tif', '*.bin', '*.txt']:
        flist = glob.glob(os.path.join(exp_folder[0], extension))
        flist.sort()
        for f in flist:
            folder, fname = os.path.split(f)
            idx = int(re.findall(r"[0-9]{4}",fname)[0])
            newname = re.sub(r"[0-9]{4}", "%04d"%(idx-1), fname)
            newfile = os.path.join(folder, newname)
            os.rename(f,newfile)
    for subf in ['MIP_XY','MIP_YZ','MIP_XZ']:
        flist = glob.glob(os.path.join(exp_folder[0], subf,'*.tif'))
        flist.sort()
        for f in flist:
            folder, fname = os.path.split(f)
            idx = int(re.findall(r"[0-9]{4}",fname)[0])
            newname = re.sub(r"[0-9]{4}", "%04d"%(idx-1), fname)
            newfile = os.path.join(folder, newname)
            os.rename(f,newfile)

    for extension in ['*.tif', '*.bin', '*.txt']:
        flist = glob.glob(os.path.join(exp_folder[1], extension))
        flist.sort()
        for f in flist:
            folder, fname = os.path.split(f)
            idx = int(re.findall(r"[0-9]{4}",fname)[0])
            newname = re.sub(r"[0-9]{4}", "%04d"%(idx-1), fname)
            newfile = os.path.join(folder, newname)
            os.rename(f,newfile)
    for subf in ['MIP_XY','MIP_YZ','MIP_XZ']:
        flist = glob.glob(os.path.join(exp_folder[1], subf,'*.tif'))
        flist.sort()
        for f in flist:
            folder, fname = os.path.split(f)
            idx = int(re.findall(r"[0-9]{4}",fname)[0])
            newname = re.sub(r"[0-9]{4}", "%04d"%(idx-1), fname)
            newfile = os.path.join(folder, newname)
            os.rename(f,newfile)
    
    # for extension in ['*.mat', '*.svb', '*.xml']:
    #     flist = glob.glob(os.path.join(exp_folder[2],'XML_finalResult_lht',extension))
    #     flist.sort()
    #     for f in flist:
    #         folder, fname = os.path.split(f)
    #         idx = int(re.findall(r"[0-9]{4}",fname)[0])
    #         newname = re.sub(r"[0-9]{4}", "%04d"%(idx-1), fname)
    #         newfile = os.path.join(folder, newname)
    #         os.rename(f,newfile)

    # print(flist[0])

    # os.rename(old_name, new_name)



