
import os, re
import pandas as pd
import numpy as np
from tqdm import tqdm
from skimage.transform import resize
from skimage.io import imread
import pyclesperanto_prototype as cle
from scipy.spatial.transform import Rotation
from scipy import optimize
from register_icp import cloud_transform
from sklearn.neighbors import NearestNeighbors

'''
Process images to find most of the cells
'''
def find_anchor_points(folder, flist, fileName = 'anchor_points.csv', 
                        scale = 1, gpu_down = 1., cell_diameter_XY = 10, isotropic = True,
                        reverse_order = True):

    anchor_file = os.path.join(folder, fileName)

    if not os.path.exists(anchor_file):
        df = pd.DataFrame({})

        for i in tqdm(range(len(flist)), total=len(flist)):
            fname = flist[i]

            img_input = imread(fname)
            
            if isotropic:
                img = cle.create([int((img_input.shape[0] * scale)/gpu_down),
                                int(img_input.shape[1]/gpu_down),
                                int(img_input.shape[2]/gpu_down)])
                cle.scale(img_input, img,
                        factor_x = 1./gpu_down,
                        factor_y = 1./gpu_down,
                        factor_z = scale/gpu_down,
                        centered = False,
                        linear_interpolation = True)
            
    #         print('Gaussian')
            sigmaxy = cell_diameter_XY/gpu_down
            sigma = [sigmaxy,sigmaxy,sigmaxy]
            
            img_gauss = cle.gaussian_blur(img, sigma_x=sigma[2], sigma_y=sigma[1], sigma_z=sigma[0])

    #         print('Find maxima')
            detected_spots = cle.detect_maxima_box(img_gauss, 
                                                radius_x=0,#sigma[2]/2, 
                                                radius_y=0,#sigma[1]/2, 
                                                radius_z=0,)#)sigma[0]/2)
            selected_spots = cle.binary_and(img_gauss>200, detected_spots)
            p = np.where(selected_spots)
    #         p = peak_local_max(img_gauss, threshold_abs=200, min_distance=5)

            df1 = pd.DataFrame({
                                'tp': int(re.findall(r"\D(\d{3})\D", " "+fname+ " ")),
                                'z': p[0].astype(float)*gpu_down,
                                'y': p[1].astype(float)*gpu_down,
                                'x': p[2].astype(float)*gpu_down,
                                })
            df = pd.concat([df,df1], ignore_index=True)
        
        df.to_csv(anchor_file, index=False)

        if reverse_order:
            df['tpreg'] = np.max(df.tp)-df.tp
        else:
            df['tpreg'] = df.tp

    else:
        df = pd.read_csv(anchor_file)

    return df

########################
'''
Registration via mean distance minimization
'''

def transformation(params):
    angles = params[:3]
    translation = params[3:]
    rot = Rotation.from_rotvec(angles).as_matrix()
    
    # special reflection case
    if np.linalg.det(rot) < 0:
        [U,S,V] = np.linalg.svd(rot)
        # multiply 3rd column of V by -1
        rot = V * U.T
    
    mat = np.eye(4)
    mat[:3,:3] = rot
    mat[:3,3] = translation
    
    return mat

def loss(params, points_ant, points_now):
    points_now_new = cloud_transform(points_now,transformation(params))
    nbrs = NearestNeighbors(n_neighbors=1, algorithm='brute').fit(points_now_new)
    distances, indices = nbrs.kneighbors(points_ant)
    return np.mean(distances)

def find_transformations(df_list, maxiter=100, tol=1e-5):

    Ts = [np.eye(4) for i in range(len(df_list))]
    for i in range(len(df_list)):
        df_list[i]['xreg'] = np.array(df_list[i]['x'])
        df_list[i]['yreg'] = np.array(df_list[i]['y'])
        df_list[i]['zreg'] = np.array(df_list[i]['z'])

    for i in tqdm(range(len(df_list)-1)):

        pos_ant = df_list[i][['z','y','x']].to_numpy()
        pos_now = df_list[i+1][['z','y','x']].to_numpy()

        initial_guess = [0,0,0,0,0,0]

        result = optimize.minimize(loss, initial_guess, 
                                args = (pos_ant, pos_now), 
                                options={'maxiter': maxiter}, tol=tol)

        Ts[i+1] = transformation(result.x)

    return Ts

def perform_transformations(df_list, Ts):
    for i in range(len(df_list)):

        A = df_list[i][['z','y','x']].to_numpy()

        T = np.eye(4)
        for j in range(i,0,-1):
            T = np.dot(Ts[j],T)
        A = cloud_transform(A, T)

        df_list[i]['zreg'] = A[:,0]
        df_list[i]['yreg'] = A[:,1]
        df_list[i]['xreg'] = A[:,2]

    return df_list