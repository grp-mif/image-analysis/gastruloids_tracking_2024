# Data managment

Description of the management of both the microscopy **input** and analysis **output** data.

- [General data management considerations](https://git.embl.de/grp-cba/cba#data-management)
- [Characters to use and avoid in filenames](https://youtu.be/dLziBlI2qlo)

## Data location

- Typically your group share

## File and folder naming scheme



