Protocol for Viventis image processing and cell tracking.

1. run preprocessing\01_image_based_registration
   point cloud-based registration
   
2. run preprocessing\02_make_tophat
   run background correction with tophat and gaussian
   
3. run TGMM

4. run matlab scripts in TGMM_processing\TGMM_matlab\compile_supervoxels.m
   conevrt xml files into mat with supervoxels for every cell detected
   
5. run TGMM_processing\01_xml2csv
   convert xml file into csv
   
6. run TGMM_processing\02_compute_fluo.py
   combines csv file and supervoxel to compute fluorescence intensity of cells

   